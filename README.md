This package is meant to evalute the performance of the `MuonsLRT` container using the n-tuples created by the the [runLRTTester](https://gitlab.cern.ch/atlas-mcp/MuonTester/-/blob/4efaf67f08633d59b3a5f7acd89b92f8cf81c75c/MuonTester/share/runLRTTester.py) job options of the [MuonTester](https://gitlab.cern.ch/atlas-mcp/MuonTester) package.
The code is based on the [NtupleAnalysisUtils](https://gitlab.cern.ch:8443/goblirsc/NtupleAnalysisUtils) framework developed by Max Goblirsch (@goblirsc).

How to setup the code
=====================
```
mkdir lrt_analyzer
cd lrt_analyzer
git clone  --recursive ssh://git@gitlab.cern.ch:7999/jojungge/largeradiusmuons.git source
mkdir build
cd build
asetup AthAnalysis,21.2.112,here
cmake ../soure && make -j64
source ${WorkDir_PLATFORM}/setup.sh
```
The recursive option in the git command is mandatory to clone also the `NtupleAnalysisUtils` code base. I guess the code would compile in `AnalysisBase` as well but that branch of the releases has been always regarded as an ancient relict which is no longer neccessary in times of cmake by me. The release version of `AthAnalysis` should not really matter, i.e. one can try a more recent version as well.


How to make the plots
=====================
Three executables have been written, to make plots for the LRT muon efficiency, the LRT muon properties and the LRT muon resolution of pt, eta and phi. 
The macros are called [PlotLRTEfficiency](https://gitlab.cern.ch/jojungge/largeradiusmuons/-/blob/master/LargeRadiusMuons/util/PlotLRTEfficiency.cxx), [PlotLRTProperties](https://gitlab.cern.ch/jojungge/largeradiusmuons/-/blob/master/LargeRadiusMuons/util/PlotLRTProperties.cxx), [PlotLRTResolution](https://gitlab.cern.ch/jojungge/largeradiusmuons/-/blob/master/LargeRadiusMuons/util/PlotLRTResolution.cxx).  All three macros  can be steered in the same way. E.g.
```
PlotLRTEfficiency
 --inFile <Path to a single file>\  # In cases of multiple files use the argument as many times
 --dir <Path to a directory where root files are stored on disk> #All files with .root are picked up
 --fileList <Path to a txt file> # The txt file contains in each line the path to a ROOT file to be analyzed
 --outDir <Path where the outplots shoud be stored>
 
``