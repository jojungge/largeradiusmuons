#ifndef LARGERADIUSMUONS__UTILS__H
#define LARGERADIUSMUONS__UTILS__H

#include <NtupleAnalysisUtils/AtlasStyle.h>
#include <NtupleAnalysisUtils/CanvasOptions.h>
#include <NtupleAnalysisUtils/DefaultPlotting.h>
#include <NtupleAnalysisUtils/Plot.h>
#include <NtupleAnalysisUtils/PlotContent.h>
#include <NtupleAnalysisUtils/PlotUtils.h>

#include "NtupleAnalysisUtils/NtupleBranch.h"

static constexpr size_t dummy_idx = -1;
/// Returns a list of files containing each the required TTree
std::vector<std::string> checkFilesForTree(const std::vector<std::string> &in_files, const std::string &tree, bool skip_empty = true);

std::string EraseWhiteSpaces(std::string str);
/// Returns true if a new line could be piped from an ifstream instance
bool GetLine(std::ifstream &inf, std::string &line);

/// Pulls over and underflows into the last and first bin
void pull_overflow(std::shared_ptr<TH1> H);

unsigned int GetNbins(const std::shared_ptr<TH1> &Histo);
unsigned int GetNbins(const TH1 *Histo);

bool isOverflowBin(const std::shared_ptr<TH1> &Histo, int bin);
bool isOverflowBin(const TH1 *Histo, int bin);
bool isOverflowBin(const TAxis *a, int bin);

bool isAlphaNumeric(std::shared_ptr<TH1> histo);
bool isAlphaNumeric(TH1 *histo);
/// Lists a direcotry where each element needs to contain the wildcard
std::vector<std::string> ListDirectory(std::string Path, const std::string &wild_card = "");

int count_non_empty(PlotContent<TH1D> &h);

#endif
