# Declare the package name:
atlas_subdir( LargeRadiusMuons )
# Declare the package's dependencies:
atlas_depends_on_subdirs(
    PUBLIC
    Event/xAOD/xAODMuon
    Event/xAOD/xAODTracking
    Event/FourMomUtils
    InnerDetector/InDetValidation/InDetPhysValMonitoring                      
    ClusterSubmission
    NtupleAnalysisUtils    
    Tools/PathResolver)

# External dependencies:
find_package( ROOT COMPONENTS Core Tree RIO Hist Physics GPad RooFit RooFitCore)

# Libraries in the package:
atlas_add_library( LargeRadiusMuonsLib
    LargeRadiusMuons/*.h Root/*.cxx
    PUBLIC_HEADERS LargeRadiusMuons
    INCLUDE_DIRS  ${ROOT_INCLUDE_DIRS}
    LINK_LIBRARIES  ${ROOT_LIBRARIES} NtupleAnalysisUtils xAODMuon       
    PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES}  PathResolver)



# Executable(s) in the package:
atlas_add_executable( PlotLRTResolution
   util/PlotLRTResolution.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} LargeRadiusMuonsLib xAODMuon)

atlas_add_executable( PlotLRTEfficiency
   util/PlotLRTEfficiency.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} LargeRadiusMuonsLib xAODMuon)

atlas_add_executable( PlotLRTProperties
   util/PlotLRTProperties.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} LargeRadiusMuonsLib xAODMuon)

# Install files from the package:
atlas_install_data( data/* )
atlas_install_data( python/*.py)
atlas_install_data( scripts/*.sh)

atlas_install_python_modules( python/*.py )
atlas_install_scripts(scripts/*.sh) 

