#include <LargeRadiusMuons/LRTAnalysis.h>

#include <fstream>
#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    HistoFiller::getDefaultFiller()->setNThreads(4);

    // Some variables for configuration
    /// Name of the ouptut file
    std::string out_dir = "efficiency_plots/";
    /// Name of the input tree
    std::string tree_name = "BasicTesterTree";
    /// Vector of the files to run
    std::vector<std::string> in_files;
    /// read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        std::string the_arg(argv[k]);
        if (the_arg.find("-t") == 0 || the_arg.find("--treeName") == 0) {
            tree_name = argv[k + 1];
            ++k;
        }
        /// Specify the input file directly
        else if (the_arg.find("-i") == 0 || the_arg.find("--inFile") == 0) {
            in_files.push_back(argv[k + 1]);
            ++k;
        }
        /// Specify a directory in which all the input files are stored
        else if (the_arg.find("-d") == 0 || the_arg.find("--dir") == 0) {
            std::string dir_path = argv[k + 1];
            std::vector<std::string> in_dir = ListDirectory(dir_path, ".root");
            in_files.insert(in_files.end(), in_dir.begin(), in_dir.end());
            ++k;
        }
        /// Specify a list of files
        else if (the_arg.find("-f") == 0 || the_arg.find("--fileList") == 0) {
            std::ifstream file_stream;
            file_stream.open(PlotUtils::GetFilePath(argv[k + 1]));
            std::string line;
            while (GetLine(file_stream, line)) { in_files.push_back(line); }
        }
        /// Change the name of the out_file
        else if (the_arg.find("-o") == 0 || the_arg.find("--outDir") == 0) {
            out_dir = argv[k + 1];
            ++k;
        }
    }

    /// Basic checks
    in_files = checkFilesForTree(in_files, tree_name);
    if (in_files.empty()) {
        std::cerr << "No files were given." << std::endl;
        return EXIT_FAILURE;
    }
    /// Create the sample handler
    Sample<LRTAnalysis> mySample("SampleNameNotUsedHere");
    for (auto& f_path : in_files) { mySample.addFile(f_path, tree_name); }
    /// Vector to cache the feature plots
    std::vector<PlotContent<TH1D>> res_variables;

    CanvasOptions canvas_opts = CanvasOptions()
                                    .yAxisTitle("a.u.")
                                    .labelStatusTag("Simulation Internal")
                                    .logY(true)
                                    .yMaxExtraPadding(0.7)
                                    .overrideOutputDir(out_dir);

    std::shared_ptr<TH1D> res_plot = std::make_shared<TH1D>("Res_Histo", "dummy;resoluition", 25, -1.25, 1.25);

    PlotPostProcessor<TH1D, TH1D> unit_integral("PullOverFlow", [](std::shared_ptr<TH1D> in) {
        pull_overflow(in);
        in->Scale(1. / in->Integral());
        return in;
    });
    auto make_res_histo = [&mySample, &res_plot, &unit_integral](std::function<float(LRTAnalysis&, size_t, LRTAnalysis::TrackType)> getter,
                                                                 LRTAnalysis::TrackType trk_type, const std::string& legend_entry,
                                                                 PlotFormat format = PlotFormat()) {
        PlotFiller1D filler(
            PlotUtils::RandomString(45), [](LRTAnalysis& t) { return t.truth_muons_pt.size(); },
            [trk_type](LRTAnalysis& t, size_t i) {
                size_t muo = t.get_lrt_muon(i);
                return muo != dummy_idx && t.lrt_link(muo, trk_type) < t.lrt_size(trk_type);
            },
            [trk_type, getter](TH1D* h, LRTAnalysis& t, size_t i) {
                float truth = getter(t, i, LRTAnalysis::Truth);
                float reco = getter(t, t.lrt_link(t.get_lrt_muon(i), trk_type), trk_type);
                h->Fill((reco - truth) / (truth != 0 ? truth : reco != 0 ? reco : 1.e8));
            },
            res_plot.get());

        return Plot<TH1D>(mySample, Cut("", [](LRTAnalysis&) { return true; }), filler, unit_integral, legend_entry, "L",
                          format.ExtraDrawOpts("HIST").FillStyle(10001).FillAlpha(0.3));
    };

    auto make_res_plot = [&make_res_histo, &res_plot, &res_variables, &canvas_opts](
                             const std::string& plot_name, const std::string& axis_title,
                             std::function<float(LRTAnalysis&, size_t, LRTAnalysis::TrackType)> getter) {
        res_plot->GetXaxis()->SetTitle(
            Form("#frac{%s^{reco} - %s^{truth}}{%s^{truth}}", axis_title.c_str(), axis_title.c_str(), axis_title.c_str()));
        res_variables.push_back(PlotContent<TH1D>(
            {
                make_res_histo(getter, LRTAnalysis::TrackType::Muon, "#mu", PlotFormat().Color(kBlue)),
                make_res_histo(getter, LRTAnalysis::TrackType::ID, "ID", PlotFormat().Color(kOrange)),
                make_res_histo(getter, LRTAnalysis::TrackType::ME, "ME", PlotFormat().Color(kCyan - 2)),
                make_res_histo(getter, LRTAnalysis::TrackType::MS, "MS", PlotFormat().Color(kRed)),
            },
            {}, {"LRT muon validation"}, "Resolution_" + plot_name, "AllResolutionPLots", canvas_opts));
    };

    make_res_plot("Pt", "p_{T} ", [](LRTAnalysis& t, size_t i, LRTAnalysis::TrackType type) { return t.get_lrt_pt(i, type); });
    make_res_plot("Eta", "#eta ", [](LRTAnalysis& t, size_t i, LRTAnalysis::TrackType type) { return t.get_lrt_eta(i, type); });
    make_res_plot("Phi", "#phi ", [](LRTAnalysis& t, size_t i, LRTAnalysis::TrackType type) { return t.get_lrt_phi(i, type); });
    make_res_plot("D0", "d_{0} ", [](LRTAnalysis& t, size_t i, LRTAnalysis::TrackType type) { return t.get_lrt_d0(i, type); });
    make_res_plot("Z0", "z_{0} ", [](LRTAnalysis& t, size_t i, LRTAnalysis::TrackType type) { return t.get_lrt_z0(i, type); });

    std::sort(res_variables.begin(), res_variables.end(),
              [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });
    PlotUtils::startMultiPagePdfFile("AllResolutionPLots", out_dir);
    for (auto& pc : res_variables) { DefaultPlotting::draw1DNoRatio(pc); }
    PlotUtils::endMultiPagePdfFile("AllResolutionPLots", out_dir);

    return EXIT_SUCCESS;
}
