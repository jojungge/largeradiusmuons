#include <LargeRadiusMuons/LRTAnalysis.h>
#include <LargeRadiusMuons/Utils.h>
#include <NtupleAnalysisUtils/PlotUtils.h>
#include <xAODMuon/Muon.h>

#include <fstream>
#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    HistoFiller::getDefaultFiller()->setNThreads(4);

    // Some variables for configuration
    /// Name of the ouptut file
    std::string out_dir = "efficiency_plots/";
    /// Name of the input tree
    std::string tree_name = "BasicTesterTree";
    /// Vector of the files to run
    std::vector<std::string> in_files;
    /// read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        std::string the_arg(argv[k]);
        if (the_arg.find("-t") == 0 || the_arg.find("--treeName") == 0) {
            tree_name = argv[k + 1];
            ++k;
        }
        /// Specify the input file directly
        else if (the_arg.find("-i") == 0 || the_arg.find("--inFile") == 0) {
            in_files.push_back(argv[k + 1]);
            ++k;
        }
        /// Specify a directory in which all the input files are stored
        else if (the_arg.find("-d") == 0 || the_arg.find("--dir") == 0) {
            std::string dir_path = argv[k + 1];
            std::vector<std::string> in_dir = ListDirectory(dir_path, ".root");
            in_files.insert(in_files.end(), in_dir.begin(), in_dir.end());
            ++k;
        }
        /// Specify a list of files
        else if (the_arg.find("-f") == 0 || the_arg.find("--fileList") == 0) {
            std::ifstream file_stream;
            file_stream.open(PlotUtils::GetFilePath(argv[k + 1]));
            std::string line;
            while (GetLine(file_stream, line)) { in_files.push_back(line); }
        }
        /// Change the name of the out_file
        else if (the_arg.find("-o") == 0 || the_arg.find("--outDir") == 0) {
            out_dir = argv[k + 1];
            ++k;
        }
    }

    /// Basic checks
    in_files = checkFilesForTree(in_files, tree_name);
    if (in_files.empty()) {
        std::cerr << "No files were given." << std::endl;
        return EXIT_FAILURE;
    }
    /// Create the sample handler
    Sample<LRTAnalysis> mySample("SampleNameNotUsedHere");
    for (auto& f_path : in_files) { mySample.addFile(f_path, tree_name); }

    /// Vector to cache the feature plots
    std::vector<PlotContent<TH1D>> effis_to_draw;

    CanvasOptions canvas_opts = CanvasOptions()
                                    .yAxisTitle("a.u.")
                                    .labelStatusTag("Simulation Internal")
                                    .logY(true)
                                    .ratioMax(1.1)
                                    .ratioMin(0.0)
                                    .ratioAxisTitle("efficiency")
                                    .yMaxExtraPadding(0.7)
                                    .overrideOutputDir(out_dir);

    /// Define the function to create the feature plots
    auto add_effi_plot = [&effis_to_draw, &mySample, &canvas_opts](
                             std::function<float(LRTAnalysis & t, size_t)> reader, std::shared_ptr<TH1> histo,
                             std::function<bool(LRTAnalysis&, size_t)> all_sel_func =
                                 [](LRTAnalysis& t, size_t i) {
                                     return std::abs(t.truth_muons_eta(i)) < 2.5 && std::abs(t.truth_muons_d0(i)) < 300;
                                 },
                             const std::string& extra_title = "") {
        /// Prompt muons
        std::function<bool(LRTAnalysis&, size_t)> prompt_mu_sel_func = [all_sel_func](LRTAnalysis& t, size_t truth_mu) {
            return all_sel_func(t, truth_mu) && t.get_prompt_muon(truth_mu) != dummy_idx;
        };
        /// Prompt loose muons
        std::function<bool(LRTAnalysis&, size_t)> prompt_loose_mu_sel_func = [all_sel_func](LRTAnalysis& t, size_t truth_mu) {
            size_t prompt_idx = t.get_prompt_muon(truth_mu);
            return all_sel_func(t, truth_mu) && prompt_idx != dummy_idx && t.muons_quality(prompt_idx) <= xAOD::Muon::Loose;
        };
        /// LRT muons
        std::function<bool(LRTAnalysis&, size_t)> lrt_muon = [all_sel_func](LRTAnalysis& t, size_t truth_mu) {
            return all_sel_func(t, truth_mu) && t.get_lrt_muon(truth_mu) != dummy_idx;
        };
        /// LRT track first
        std::function<bool(LRTAnalysis&, size_t)> lrt_track = [all_sel_func](LRTAnalysis& t, size_t truth_mu) {
            return all_sel_func(t, truth_mu) && t.get_lrt_track(truth_mu) != dummy_idx;
        };

        std::function<bool(LRTAnalysis&, size_t)> id_track = [all_sel_func](LRTAnalysis& t, size_t truth_mu) {
            return all_sel_func(t, truth_mu) && t.get_id_track(truth_mu) != dummy_idx;
        };
        /// Make sure to normalize the plots to unity

        auto make_plot = [&mySample, &histo, &reader](std::function<bool(LRTAnalysis&, size_t)> sel_func, const std::string& legend_item,
                                                      PlotFormat format = PlotFormat()) {
            static const PlotPostProcessor<TH1D, TH1D> overflow_pull("PullOverFlow", [](std::shared_ptr<TH1D> in) {
                pull_overflow(in);
                return in;
            });
            /// Count the truth muon container size
            static std::function<size_t(LRTAnalysis&)> size_func = [](LRTAnalysis& t) { return t.truth_muons_pt.size(); };

            PlotFiller1D filler(
                PlotUtils::RandomString(45), size_func, sel_func, [reader](TH1D* h, LRTAnalysis& t, size_t i) { h->Fill(reader(t, i)); },
                dynamic_cast<TH1D*>(histo.get()));

            return Plot<TH1D>(mySample, Cut("", [](LRTAnalysis&) { return true; }), filler, overflow_pull, legend_item, "PL",
                              format.ExtraDrawOpts("HIST"));
        };

        Plot<TH1D> all_plot = make_plot(all_sel_func, "all", PlotFormat().Color(kBlack));

        Plot<TH1D> ltrmu_plot = make_plot(lrt_muon, "LRT muon", PlotFormat().Color(kOrange).FillStyle(10001).FillAlpha(0.5));
        Plot<TH1D> lrt_id_plot = make_plot(lrt_track, "LRT ID track", PlotFormat().Color(kRed).FillStyle(10001).FillAlpha(0.5));

        effis_to_draw.push_back(PlotContent<TH1D>(
            {all_plot,
             make_plot(prompt_loose_mu_sel_func, "prompt muon (Loose)", PlotFormat().Color(kBlue).FillStyle(10001).FillAlpha(0.5)),
             ltrmu_plot},
            {RatioEntry(1, 0, PlotUtils::efficiencyErrors), RatioEntry(2, 0, PlotUtils::efficiencyErrors)

            },
            ///
            {"LRT muon validation", extra_title}, "EfficiencyPlot_" + std::string(histo->GetName()) + "_Muon", "AllEfficiencyPlots",
            canvas_opts));

        effis_to_draw.push_back(PlotContent<TH1D>(
            {

                all_plot, make_plot(prompt_mu_sel_func, "prompt muon track", PlotFormat().Color(kCyan - 2).FillStyle(10001).FillAlpha(0.5)),
                make_plot(id_track, "Prompt ID track", PlotFormat().Color(kViolet + 1).FillStyle(10001).FillAlpha(0.5)), lrt_id_plot},
            {
                RatioEntry(1, 0, PlotUtils::efficiencyErrors),
                RatioEntry(2, 0, PlotUtils::efficiencyErrors),
                RatioEntry(3, 0, PlotUtils::efficiencyErrors),
            },
            ///
            {"LRT muon validation", extra_title}, "EfficiencyPlot_" + std::string(histo->GetName()) + "_Tracks", "AllEfficiencyPlots",
            canvas_opts));

        effis_to_draw.push_back(PlotContent<TH1D>({lrt_id_plot, ltrmu_plot}, {RatioEntry(1, 0, PlotUtils::efficiencyErrors)},
                                                  ///
                                                  {"LRT muon validation"}, "EfficiencyPlot_" + std::string(histo->GetName()) + "_TrackMuon",
                                                  "AllEfficiencyPlots", canvas_opts));
    };

    /// Call the function to create the feature plots
    /// The first argument is the name of the plot
    /// The second one is the instruction how the histograms are going to be filled
    /// The next arguments are constructing the histogram
    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_pt(i); },
                  std::make_shared<TH1D>("Pt", "dummy;p_{T} [GeV];a.u.", 20, 0, 200));
    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_eta(i); },
                  std::make_shared<TH1D>("Eta", "dummy;#eta;a.u.", 27, -2.7, 2.7));
    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_phi(i); },
                  std::make_shared<TH1D>("Phi", "dummy;#phi;a.u.", 30, -std::acos(0), std::acos(0)));
    for (float min_d0 : {33.5, 50.5, 88.5, 122.5}) {
        add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_pt(i); },
                      std::make_shared<TH1D>(Form("Pt_D0_%.1f", min_d0), "dummy;p_{T} [GeV];a.u.", 20, 0, 200),
                      [min_d0](LRTAnalysis& t, size_t i) {
                          return std::abs(t.truth_muons_eta(i)) < 2.5 && std::abs(t.truth_muons_d0(i)) > 0 && min_d0 &&
                                 std::abs(t.truth_muons_d0(i)) < 300;
                      },
                      Form("d_{0} > %.1f mm", min_d0));
        add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_eta(i); },
                      std::make_shared<TH1D>(Form("Eta_D0_%.1f", min_d0), "dummy;#eta;a.u.", 27, -2.7, 2.7),
                      [min_d0](LRTAnalysis& t, size_t i) {
                          return std::abs(t.truth_muons_eta(i)) < 2.5 && std::abs(t.truth_muons_d0(i)) > 0 && min_d0 &&
                                 std::abs(t.truth_muons_d0(i)) < 300;
                      },
                      Form("d_{0} > %.1f mm", min_d0));
        add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_phi(i); },
                      std::make_shared<TH1D>(Form("Phi_D0_%.1f", min_d0), "dummy;#phi;a.u.", 30, -std::acos(0), std::acos(0)),
                      [min_d0](LRTAnalysis& t, size_t i) {
                          return std::abs(t.truth_muons_eta(i)) < 2.5 && std::abs(t.truth_muons_d0(i)) > 0 && min_d0 &&
                                 std::abs(t.truth_muons_d0(i)) < 300;
                      },
                      Form("d_{0} > %.1f mm", min_d0));

        add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_pt(i); },
                      std::make_shared<TH1D>(Form("Pt_D0_eq_%.1f", min_d0), "dummy;p_{T} [GeV];a.u.", 20, 0, 200),
                      [min_d0](LRTAnalysis& t, size_t i) {
                          return std::abs(t.truth_muons_eta(i)) < 2.5 && std::abs(t.truth_muons_d0(i) - min_d0) < 0.5;
                      },
                      Form("d_{0} = %.1f#pm 0.5 mm", min_d0));
        add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_eta(i); },
                      std::make_shared<TH1D>(Form("Eta_D0_eq_%.1f", min_d0), "dummy;#eta;a.u.", 27, -2.7, 2.7),
                      [min_d0](LRTAnalysis& t, size_t i) {
                          return std::abs(t.truth_muons_eta(i)) < 2.5 && std::abs(t.truth_muons_d0(i) - min_d0) < 0.5;
                      },
                      Form("d_{0} > %.1f#pm 0.5 mm", min_d0));
        add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_phi(i); },
                      std::make_shared<TH1D>(Form("Phi_D0_eq_%.1f", min_d0), "dummy;#phi;a.u.", 30, -std::acos(0), std::acos(0)),
                      [min_d0](LRTAnalysis& t, size_t i) {
                          return std::abs(t.truth_muons_eta(i)) < 2.5 && std::abs(t.truth_muons_d0(i) - min_d0) < 0.5;
                      },
                      Form("d_{0} > %.1f#pm 0.5 mm", min_d0));
    }

    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return std::abs(t.truth_muons_d0(i)); },
                  std::make_shared<TH1D>("D0", "dummy;d_{0} [mm];a.u.", 50, 0, 300));

    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return std::abs(t.truth_muons_Rprod(i)); },
                  std::make_shared<TH1D>("DProd", "dummy;R_{prod} [mm];a.u.", 50, 0, 300));

    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return std::abs(t.truth_muons_z0(i) / std::cosh(t.truth_muons_eta(i))); },
                  std::make_shared<TH1D>("Z0SinTheta", "dummy;z_{0} sin#theta [mm];a.u.", 50, 0, 25));

    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return std::abs(t.truth_muons_Zprod(i) / std::cosh(t.truth_muons_eta(i))); },
                  std::make_shared<TH1D>("ZProdSinTheta", "dummy;z_{prod} sin#theta [mm];a.u.", 50, 0, 25));
    add_effi_plot([](LRTAnalysis& t, size_t i) -> float { return t.truth_muons_q(i); },
                  std::make_shared<TH1D>("charge", "dummy;q_{#mu};a.u.", 2, -1, 1));

    std::sort(effis_to_draw.begin(), effis_to_draw.end(),
              [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });
    PlotUtils::startMultiPagePdfFile("AllEfficiencyPlots", out_dir);
    for (auto& pc : effis_to_draw) {
        if (count_non_empty(pc)) { DefaultPlotting::draw1D(pc); }
    }
    PlotUtils::endMultiPagePdfFile("AllEfficiencyPlots", out_dir);

    return EXIT_SUCCESS;
}
