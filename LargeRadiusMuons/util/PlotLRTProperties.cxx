#include <LargeRadiusMuons/LRTAnalysis.h>
#include <LargeRadiusMuons/Utils.h>
#include <NtupleAnalysisUtils/PlotUtils.h>
#include <xAODMuon/Muon.h>

#include <fstream>
#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
    SetAtlasStyle();
    HistoFiller::getDefaultFiller()->setNThreads(4);

    // Some variables for configuration
    /// Name of the ouptut file
    std::string out_dir = "efficiency_plots/";
    /// Name of the input tree
    std::string tree_name = "BasicTesterTree";
    /// Vector of the files to run
    std::vector<std::string> in_files;
    /// read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        std::string the_arg(argv[k]);
        if (the_arg.find("-t") == 0 || the_arg.find("--treeName") == 0) {
            tree_name = argv[k + 1];
            ++k;
        }
        /// Specify the input file directly
        else if (the_arg.find("-i") == 0 || the_arg.find("--inFile") == 0) {
            in_files.push_back(argv[k + 1]);
            ++k;
        }
        /// Specify a directory in which all the input files are stored
        else if (the_arg.find("-d") == 0 || the_arg.find("--dir") == 0) {
            std::string dir_path = argv[k + 1];
            std::vector<std::string> in_dir = ListDirectory(dir_path, ".root");
            in_files.insert(in_files.end(), in_dir.begin(), in_dir.end());
            ++k;
        }
        /// Specify a list of files
        else if (the_arg.find("-f") == 0 || the_arg.find("--fileList") == 0) {
            std::ifstream file_stream;
            file_stream.open(PlotUtils::GetFilePath(argv[k + 1]));
            std::string line;
            while (GetLine(file_stream, line)) { in_files.push_back(line); }
        }
        /// Change the name of the out_file
        else if (the_arg.find("-o") == 0 || the_arg.find("--outDir") == 0) {
            out_dir = argv[k + 1];
            ++k;
        }
    }

    /// Basic checks
    in_files = checkFilesForTree(in_files, tree_name);
    if (in_files.empty()) {
        std::cerr << "No files were given." << std::endl;
        return EXIT_FAILURE;
    }
    /// Create the sample handler
    Sample<LRTAnalysis> mySample("SampleNameNotUsedHere");
    for (auto& f_path : in_files) { mySample.addFile(f_path, tree_name); }

    /// Vector to cache the feature plots
    std::vector<PlotContent<TH1D>> obs_to_draw;

    CanvasOptions canvas_opts = CanvasOptions()
                                    .yAxisTitle("a.u.")
                                    .labelStatusTag("Simulation Internal")
                                    .logY(true)
                                    // ratioMax(1.1)
                                    //.ratioMin(0.0)
                                    //.ratioAxisTitle("efficiency")
                                    .yMaxExtraPadding(0.7)
                                    .overrideOutputDir(out_dir);
    PlotPostProcessor<TH1D, TH1D> unit_integral("PullOverFlow", [](std::shared_ptr<TH1D> in) {
        pull_overflow(in);
        in->Scale(1. / in->Integral());
        return in;
    });

    /// Define the function to create the feature plots
    auto add_property_plot = [&obs_to_draw, &mySample, &canvas_opts, &unit_integral](
                                 std::shared_ptr<TH1> histo, std::function<float(LRTAnalysis & t, size_t)> reader,
                                 std::function<bool(LRTAnalysis&, size_t)> sel_func = [](LRTAnalysis&, size_t) { return true; },
                                 const std::string& extra_title = "") {
        static std::function<size_t(LRTAnalysis&)> size_func = [](LRTAnalysis& t) { return t.LRT_muons_IDLink.size(); };
        PlotFiller1D filler(
            PlotUtils::RandomString(45), size_func, sel_func, [reader](TH1D* h, LRTAnalysis& t, size_t i) { h->Fill(reader(t, i)); },
            dynamic_cast<TH1D*>(histo.get()));
        PlotFiller1D filler_truth(
            PlotUtils::RandomString(45), size_func,
            [sel_func](LRTAnalysis& t, size_t i) { return sel_func(t, i) && t.get_truth_lrt(i) < t.truth_muons_q.size(); },
            [reader](TH1D* h, LRTAnalysis& t, size_t i) { h->Fill(reader(t, i)); }, dynamic_cast<TH1D*>(histo.get()));

        obs_to_draw.push_back(PlotContent<TH1D>(
            {Plot<TH1D>(mySample, Cut("", [](LRTAnalysis&) { return true; }), filler, unit_integral, "LRT #mu", "L",
                        PlotFormat().ExtraDrawOpts("HIST").Color(kOrange).FillStyle(10001).FillAlpha(0.5)),
             Plot<TH1D>(mySample, Cut("", [](LRTAnalysis&) { return true; }), filler_truth, unit_integral, "LRT #mu (truth matched)", "L",
                        PlotFormat().ExtraDrawOpts("HIST").Color(kBlue).FillStyle(10001).FillAlpha(0.5))},
            {}, {"LRT muon validation", extra_title},
            "Property_" +
                (extra_title.size()
                     ? PlotUtils::pruneUnwantedCharacters(extra_title, {" ", "[", "]", "(", ")", "{", "}", "/", ":", "#", "^", "<"})
                     : "") +
                std::string(histo->GetName()),
            "AllPropertyPlots", canvas_opts));
    };

    /// Call the function to create the feature plots
    /// The first argument is the name of the plot
    /// The second one is the instruction how the histograms are going to be filled
    /// The next arguments are constructing the histogram
    auto make_muon_plot_set = [add_property_plot](
                                  std::function<bool(LRTAnalysis&, size_t)> sel_func = [](LRTAnalysis&, size_t) { return true; },
                                  const std::string& extra_title = "") {
        add_property_plot(
            std::make_shared<TH1D>("Pt", "dummy;p_{T} [GeV];a.u.", 20, 0, 200),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_muons_pt(i); }, sel_func, extra_title);

        add_property_plot(
            std::make_shared<TH1D>("Eta", "dummy;#eta;a.u.", 55, -2.75, 2.75),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_muons_eta(i); }, sel_func, extra_title);

        add_property_plot(
            std::make_shared<TH1D>("Eta", "dummy;#phi [rad];a.u.", 30, -3.15, 3.15),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_muons_phi(i); }, sel_func, extra_title);

        add_property_plot(
            std::make_shared<TH1D>("Chi_nDOF", "dummy;#chi^{2} / nDoF;a.u.", 16, 0, 16),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_PrimaryTracks_chi2(i) / t.LRT_PrimaryTracks_nDoF(i); }, sel_func,
            extra_title);

        add_property_plot(
            std::make_shared<TH1D>("D0", "dummy; d_{0} [mm];a.u.", 50, 0, 300),
            [](LRTAnalysis& t, size_t i) -> float { return std::abs(t.LRT_PrimaryTracks_d0(i)); }, sel_func, extra_title);

        add_property_plot(
            std::make_shared<TH1D>("N_PixelHits", "dummy; N_{hits}^{pixel};a.u.", 12, 0, 12),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfPixelHits(i);
            },
            sel_func, extra_title);
        add_property_plot(
            std::make_shared<TH1D>("N_PixelHoles", "dummy; N_{holes}^{pixel};a.u.", 5, 0, 5),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfPixelHoles(i);
            },
            sel_func, extra_title);
        add_property_plot(
            std::make_shared<TH1D>("N_PixelOutliers", "dummy; N_{outliers}^{pixel};a.u.", 5, 0, 5),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfPixelOutliers(i);
            },
            sel_func, extra_title);
        /// SCT
        add_property_plot(
            std::make_shared<TH1D>("N_SCTHits", "dummy; N_{hits}^{SCT};a.u.", 16, 0, 16),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfSCTHits(i);
            },
            sel_func, extra_title);
        add_property_plot(
            std::make_shared<TH1D>("N_SCTHoles", "dummy; N_{holes}^{SCT};a.u.", 5, 0, 5),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfSCTHoles(i);
            },
            sel_func, extra_title);
        add_property_plot(
            std::make_shared<TH1D>("N_SCTOutLiers", "dummy; N_{outliers}^{SCT};a.u.", 5, 0, 5),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfSCTOutliers(i);
            },
            sel_func, extra_title);
        /// TRT
        add_property_plot(
            std::make_shared<TH1D>("N_TRTHits", "dummy; N_{hits}^{TRT};a.u.", 50, 0, 50),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfTRTHits(i);
            },
            sel_func, extra_title);
        add_property_plot(
            std::make_shared<TH1D>("N_TRTOutLiers", "dummy; N_{outliers}^{TRT};a.u.", 5, 0, 5),
            [](LRTAnalysis& t, size_t i) -> float {
                i = t.LRT_muons_IDLink(i);
                return t.LRT_IDTracks_numberOfTRTOutliers(i);
            },
            sel_func, extra_title);

        add_property_plot(
            std::make_shared<TH1D>("Rho_Prime", "dummy; #rho^{'}_{ME};a.u.", 51, -1.05, 1.05),
            [](LRTAnalysis& t, size_t i) -> float {
                size_t id = t.LRT_muons_IDLink(i);
                size_t me = t.LRT_muons_MELink(i);
                return (t.LRT_METracks_pt(me) - t.LRT_IDTracks_pt(id)) / t.LRT_muons_pt(i);
            },
            [sel_func](LRTAnalysis& t, size_t i) { return sel_func(t, i) && t.LRT_muons_MELink(i) < t.LRT_METracks_pt.size(); },
            extra_title);
        add_property_plot(
            std::make_shared<TH1D>("Rho_PrimeMS", "dummy; #rho^{'}_{MS};a.u.", 51, -1.05, 1.05),
            [](LRTAnalysis& t, size_t i) -> float {
                size_t id = t.LRT_muons_IDLink(i);
                size_t me = t.LRT_muons_MSLink(i);
                return (t.LRT_MSTracks_pt(me) - t.LRT_IDTracks_pt(id)) / t.LRT_muons_pt(i);
            },
            [sel_func](LRTAnalysis& t, size_t i) { return sel_func(t, i) && t.LRT_muons_MSLink(i) < t.LRT_METracks_pt.size(); },
            extra_title);
        add_property_plot(
            std::make_shared<TH1D>("Rho_PrimeMEOE", "dummy; #rho^{'}_{MEOE};a.u.", 51, -1.05, 1.05),
            [](LRTAnalysis& t, size_t i) -> float {
                size_t id = t.LRT_muons_IDLink(i);
                size_t me = t.LRT_muons_MSOELink(i);
                return (t.LRT_MSOETracks_pt(me) - t.LRT_IDTracks_pt(id)) / t.LRT_muons_pt(i);
            },
            [sel_func](LRTAnalysis& t, size_t i) { return sel_func(t, i) && t.LRT_muons_MSOELink(i) < t.LRT_muons_MSOELink.size(); },
            extra_title);

        /// Muon type plot
        std::shared_ptr<TH1D> type_histo = std::make_shared<TH1D>("type", "dummy;#mu-type;a.u.", 5, 0, 5);
        type_histo->GetXaxis()->SetBinLabel(1, "CB");
        type_histo->GetXaxis()->SetBinLabel(2, "SA");
        type_histo->GetXaxis()->SetBinLabel(3, "ST");
        type_histo->GetXaxis()->SetBinLabel(4, "CT");
        type_histo->GetXaxis()->SetBinLabel(5, "SAF");
        add_property_plot(
            type_histo, [](LRTAnalysis& t, size_t i) -> float { return t.LRT_muons_type(i); }, sel_func, extra_title);

        std::shared_ptr<TH1D> author_histo = std::make_shared<TH1D>("author", "dummy;#mu-author;a.u.", 10, 1, 11);
        author_histo->GetXaxis()->SetBinLabel(1, "MuidCo");
        author_histo->GetXaxis()->SetBinLabel(2, "STACO");
        author_histo->GetXaxis()->SetBinLabel(3, "MuTag");
        author_histo->GetXaxis()->SetBinLabel(4, "MuTagIMO");
        author_histo->GetXaxis()->SetBinLabel(5, "MuidSA");
        author_histo->GetXaxis()->SetBinLabel(6, "MuGirl");
        author_histo->GetXaxis()->SetBinLabel(7, "MuGirl (#beta)");
        author_histo->GetXaxis()->SetBinLabel(8, "CaloTag");
        author_histo->GetXaxis()->SetBinLabel(9, "CaloLikelihood");
        author_histo->GetXaxis()->SetBinLabel(10, "ExtrapolateMuonToIP");
        add_property_plot(
            author_histo, [](LRTAnalysis& t, size_t i) -> float { return t.LRT_muons_author(i); }, sel_func, extra_title);
    };

    make_muon_plot_set();
    make_muon_plot_set([](LRTAnalysis& t, size_t i) { return t.LRT_PrimaryTracks_chi2(i) / t.LRT_PrimaryTracks_nDoF(i) < 10.; },
                       "#chi^{2}/nDoF < 10");
    make_muon_plot_set([](LRTAnalysis& t, size_t i) { return t.LRT_muons_author(i) != xAOD::Muon::Author::STACO; }, "no STACO");
    make_muon_plot_set(
        [](LRTAnalysis& t, size_t i) {
            return t.LRT_muons_author(i) != xAOD::Muon::Author::STACO && t.LRT_PrimaryTracks_chi2(i) / t.LRT_PrimaryTracks_nDoF(i) < 10.;
        },
        "no STACO, #chi^{2}/nDoF < 10");

    /// Lets' come to some ID tracks properties and variables
    auto add_id_lrt_plot = [&obs_to_draw, &mySample, &canvas_opts, &unit_integral](
                               std::shared_ptr<TH1> histo, std::function<float(LRTAnalysis & t, size_t)> reader,
                               std::function<bool(LRTAnalysis&, size_t)> sel_func = [](LRTAnalysis&, size_t) { return true; },
                               const std::string& extra_title = "") {
        static std::function<size_t(LRTAnalysis&)> size_func = [](LRTAnalysis& t) { return t.LRT_IDTracks_truthLink.size(); };
        PlotFiller1D filler(
            PlotUtils::RandomString(45), size_func, sel_func, [reader](TH1D* h, LRTAnalysis& t, size_t i) { h->Fill(reader(t, i)); },
            dynamic_cast<TH1D*>(histo.get()));
        PlotFiller1D filler_truth(
            PlotUtils::RandomString(45), size_func,
            [sel_func](LRTAnalysis& t, size_t i) { return sel_func(t, i) && t.LRT_IDTracks_truthLink(i) < t.truth_muons_q.size(); },
            [reader](TH1D* h, LRTAnalysis& t, size_t i) { h->Fill(reader(t, i)); }, dynamic_cast<TH1D*>(histo.get()));

        obs_to_draw.push_back(PlotContent<TH1D>(
            {Plot<TH1D>(mySample, Cut("", [](LRTAnalysis&) { return true; }), filler, unit_integral, "LRT IDTrack", "L",
                        PlotFormat().ExtraDrawOpts("HIST").Color(kCyan - 2).FillStyle(10001).FillAlpha(0.5)),
             Plot<TH1D>(mySample, Cut("", [](LRTAnalysis&) { return true; }), filler_truth, unit_integral, "LRT IDTrack (truth matched)",
                        "L", PlotFormat().ExtraDrawOpts("HIST").Color(kMagenta - 4).FillStyle(10001).FillAlpha(0.5))},
            {}, {"LRT muon validation", extra_title}, "Property_ID" + std::string(histo->GetName()), "AllPropertyPlots", canvas_opts));
    };

    add_id_lrt_plot(std::make_shared<TH1D>("Pt", "dummy;p_{T} [GeV];a.u.", 20, 0, 200),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_pt(i); });

    add_id_lrt_plot(std::make_shared<TH1D>("Eta", "dummy;#eta;a.u.", 51, -2.55, 2.55),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_eta(i); });

    add_id_lrt_plot(std::make_shared<TH1D>("Phi", "dummy;#phi [rad];a.u.", 30, -3.15, 3.15),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_phi(i); });

    add_id_lrt_plot(std::make_shared<TH1D>("Chi_nDOF", "dummy;#chi^{2} / nDoF;a.u.", 16, 0, 16),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_chi2(i) / t.LRT_IDTracks_nDoF(i); });

    add_id_lrt_plot(std::make_shared<TH1D>("D0", "dummy; d_{0} [mm];a.u.", 50, 0, 300),
                    [](LRTAnalysis& t, size_t i) -> float { return std::abs(t.LRT_IDTracks_d0(i)); });

    add_id_lrt_plot(std::make_shared<TH1D>("N_PixelHits", "dummy; N_{hits}^{pixel};a.u.", 12, 0, 12),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfPixelHits(i); });
    add_id_lrt_plot(std::make_shared<TH1D>("N_PixelHoles", "dummy; N_{holes}^{pixel};a.u.", 5, 0, 5),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfPixelHoles(i); });
    add_id_lrt_plot(std::make_shared<TH1D>("N_PixelOutliers", "dummy; N_{outliers}^{pixel};a.u.", 5, 0, 5),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfPixelOutliers(i); });
    /// SCT
    add_id_lrt_plot(std::make_shared<TH1D>("N_SCTHits", "dummy; N_{hits}^{SCT};a.u.", 16, 0, 16),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfSCTHits(i); });
    add_id_lrt_plot(std::make_shared<TH1D>("N_SCTHoles", "dummy; N_{holes}^{SCT};a.u.", 5, 0, 5),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfSCTHoles(i); });
    add_id_lrt_plot(std::make_shared<TH1D>("N_SCTOutLiers", "dummy; N_{outliers}^{SCT};a.u.", 5, 0, 5),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfSCTOutliers(i); });

    add_id_lrt_plot(std::make_shared<TH1D>("N_TRTHits", "dummy; N_{hits}^{TRT};a.u.", 50, 0, 50),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfTRTHits(i); });
    add_id_lrt_plot(std::make_shared<TH1D>("N_TRTOutLiers", "dummy; N_{outliers}^{TRT};a.u.", 5, 0, 5),
                    [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_numberOfTRTOutliers(i); });
    ///
    ///     Insert d0 cuts
    ///
    for (float min_d0 : {33.5, 50.5, 88.5, 122.5}) {
        add_id_lrt_plot(
            std::make_shared<TH1D>(Form("Pt_D0_EQ_%.1f", min_d0), "dummy;p_{T} [GeV];a.u.", 20, 0, 200),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_pt(i); },
            [min_d0](LRTAnalysis& t, size_t i) { return std::abs(min_d0 - t.LRT_IDTracks_d0(i)) < 0.5; },
            Form("d_{0} = %1.f#pm0.5 mm", min_d0));

        add_id_lrt_plot(
            std::make_shared<TH1D>(Form("Pt_D0_Gtr_%.1f", min_d0), "dummy;p_{T} [GeV];a.u.", 20, 0, 200),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_pt(i); },
            [min_d0](LRTAnalysis& t, size_t i) { return std::abs(t.LRT_IDTracks_d0(i)) > min_d0; }, Form("d_{0} > %1.f mm", min_d0));

        add_id_lrt_plot(
            std::make_shared<TH1D>(Form("Eta_D0_EQ_%.1f", min_d0), "dummy;#eta;a.u.", 51, -2.55, 2.55),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_eta(i); },
            [min_d0](LRTAnalysis& t, size_t i) { return std::abs(min_d0 - t.LRT_IDTracks_d0(i)) < 0.5; },
            Form("d_{0} = %1.f#pm0.5 mm", min_d0));

        add_id_lrt_plot(
            std::make_shared<TH1D>(Form("Eta_D0_Gtr_%.1f", min_d0), "dummy;#eta;a.u.", 51, -2.55, 2.55),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_eta(i); },
            [min_d0](LRTAnalysis& t, size_t i) { return std::abs(t.LRT_IDTracks_d0(i)) > min_d0; }, Form("d_{0} > %1.f mm", min_d0));

        add_id_lrt_plot(
            std::make_shared<TH1D>(Form("Phi_D0_EQ_%.1f", min_d0), "dummy;#phi [rad];a.u.", 30, -3.15, 3.15),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_phi(i); },
            [min_d0](LRTAnalysis& t, size_t i) { return std::abs(min_d0 - t.LRT_IDTracks_d0(i)) < 0.5; },
            Form("d_{0} = %1.f#pm0.5 mm", min_d0));

        add_id_lrt_plot(
            std::make_shared<TH1D>(Form("Phi_D0_Gtr_%.1f", min_d0), "dummy;#phi [rad];a.u.", 30, -3.15, 3.15),
            [](LRTAnalysis& t, size_t i) -> float { return t.LRT_IDTracks_phi(i); },
            [min_d0](LRTAnalysis& t, size_t i) { return std::abs(t.LRT_IDTracks_d0(i)) > min_d0; }, Form("d_{0} > %1.f mm", min_d0));
    }
    std::sort(obs_to_draw.begin(), obs_to_draw.end(),
              [](const PlotContent<TH1D>& a, const PlotContent<TH1D>& b) { return a.getFileName() < b.getFileName(); });
    PlotUtils::startMultiPagePdfFile("AllPropertyPlots", out_dir);
    for (auto& pc : obs_to_draw) {
        if (count_non_empty(pc)) { DefaultPlotting::draw1D(pc); }
    }
    PlotUtils::endMultiPagePdfFile("AllPropertyPlots", out_dir);

    return EXIT_SUCCESS;
}
