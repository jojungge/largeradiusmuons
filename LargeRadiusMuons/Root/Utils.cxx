// Please add your package name here
#include <LargeRadiusMuons/Utils.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2Poly.h>
#include <TTree.h>
#include <dirent.h>

#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
std::vector<std::string> checkFilesForTree(const std::vector<std::string> &in_files, const std::string &tree, bool skip_empty) {
    std::vector<std::string> out_files;
    out_files.reserve(in_files.size());
    if (tree.empty()) {
        std::cerr << "ERROR: -- Empty tree name given" << std::endl;
        return out_files;
    }
    for (auto &in : in_files) {
        std::shared_ptr<TFile> f = PlotUtils::Open(in);
        if (!f || !f->IsOpen()) continue;
        TTree *t = nullptr;
        f->GetObject(tree.c_str(), t);
        if (!t) continue;
        if (!skip_empty || t->GetEntries()) { out_files.push_back(PlotUtils::GetFilePath(in)); }
        delete t;
    }
    return out_files;
}
bool GetLine(std::ifstream &inf, std::string &line) {
    if (!std::getline(inf, line)) return false;
    line = EraseWhiteSpaces(line);
    if (line.find("#") == 0 || line.size() < 1) return GetLine(inf, line);
    return true;
}
std::vector<std::string> ListDirectory(std::string Path, const std::string &WildCard) {
    std::vector<std::string> List;
    DIR *dir;
    dirent *ent;
    std::cout << "ListDirectory() --- INFO: Read content of directory " << std::endl;
    if ((dir = opendir(Path.c_str())) != nullptr) {
        while ((ent = readdir(dir)) != nullptr) {
            std::string Entry = ent->d_name;
            if (Entry == "." || Entry == "..") continue;  // skip the .. and . lines in ls
            if (WildCard.size() == 0 || Entry.find(WildCard) < Entry.size()) List.push_back(Path + "/" + Entry);
        }
        closedir(dir);
    } else {
        std::cerr << "ListDirectory() --- ERROR: No such file or directory " << Path << std::endl;
    }
    return List;
}

unsigned int GetNbins(const std::shared_ptr<TH1> &Histo) { return GetNbins(Histo.get()); }
unsigned int GetNbins(const TH1 *Histo) {
    if (Histo == nullptr) return 0;
    if (Histo->GetDimension() == 2) {
        const TH2Poly *poly = dynamic_cast<const TH2Poly *>(Histo);
        if (poly != nullptr) return poly->GetNumberOfBins() + 1;
    }
    unsigned int N = Histo->GetNbinsX() + 2;
    if (Histo->GetDimension() >= 2) N *= (Histo->GetNbinsY() + 2);
    if (Histo->GetDimension() == 3) N *= (Histo->GetNbinsZ() + 2);
    return N;
}
bool isOverflowBin(const std::shared_ptr<TH1> &Histo, int bin) { return isOverflowBin(Histo.get(), bin); }
bool isOverflowBin(const TH1 *Histo, int bin) {
    int x(-1), y(-1), z(-1);
    Histo->GetBinXYZ(bin, x, y, z);
    if (isOverflowBin(Histo->GetXaxis(), x)) return true;
    if (Histo->GetDimension() >= 2 && isOverflowBin(Histo->GetYaxis(), y)) return true;
    if (Histo->GetDimension() == 3 && isOverflowBin(Histo->GetZaxis(), z)) return true;
    return false;
}
bool isOverflowBin(const TAxis *a, int bin) { return bin <= 0 || bin >= a->GetNbins() + 1; }
void pull_overflow(std::shared_ptr<TH1> H) {
    if (isAlphaNumeric(H)) return;
    std::function<int(TAxis *, int)> shift_bin = [](TAxis *A, int bin) {
        if (bin == 0) return 1;
        if (isOverflowBin(A, bin)) return bin - 1;
        return bin;
    };
    for (int b = GetNbins(H) - 1; b >= 0; --b) {
        if (!isOverflowBin(H, b)) continue;
        int x(0), y(0), z(0);
        H->GetBinXYZ(b, x, y, z);
        /// Determine the target X
        int t_x(shift_bin(H->GetXaxis(), x)), t_y(y), t_z(z);
        if (H->GetDimension() > 1) t_y = shift_bin(H->GetYaxis(), y);
        if (H->GetDimension() > 2) t_z = shift_bin(H->GetZaxis(), z);
        int target_bin = H->GetBin(t_x, t_y, t_z);
        H->SetBinContent(target_bin, H->GetBinContent(target_bin) + H->GetBinContent(b));
        H->SetBinError(target_bin, std::hypot(H->GetBinError(target_bin), H->GetBinError(b)));
        H->SetBinContent(b, 0);
        H->SetBinError(b, 0);
    }
}
std::string EraseWhiteSpaces(std::string str) {
    str.erase(std::remove(str.begin(), str.end(), '\t'), str.end());
    while (!str.empty() && std::find_if(str.begin(), str.end(), [](unsigned char c) { return std::isspace(c); }) == str.begin())
        str = str.substr(1, std::string::npos);
    while (!str.empty() && std::find_if(str.rbegin(), str.rend(), [](unsigned char c) { return std::isspace(c); }) == str.rbegin())
        str = str.substr(0, str.size() - 1);
    return str;
}
bool isAlphaNumeric(std::shared_ptr<TH1> histo) { return isAlphaNumeric(histo.get()); }
bool isAlphaNumeric(TH1 *histo) {
    if (histo->GetXaxis()->IsAlphanumeric()) { return true; }
    if (histo->GetDimension() >= 2 && histo->GetYaxis()->IsAlphanumeric()) { return true; }
    if (histo->GetDimension() == 3 && histo->GetZaxis()->IsAlphanumeric()) { return true; }
    return false;
}
int count_non_empty(PlotContent<TH1D> &h) {
    h.populateAll();
    unsigned int n = 0;

    for (auto &pl : h.getPlots())
        if (pl->GetEntries() > 0) ++n;
    return n;
}
