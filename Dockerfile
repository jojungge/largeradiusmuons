FROM atlas/athanalysis:21.2.137
ADD . /jojungge/LargeRadiusMuons
WORKDIR /jojungge/build
RUN source ~/release_setup.sh &&  sudo chown -R atlas /jojungge && cmake ../LargeRadiusMuons &&  make 
